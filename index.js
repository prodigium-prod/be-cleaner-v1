const express = require('express')
const {response} = require("./src/middleware/common");
require("dotenv").config();
const bodyParser = require('body-parser')
const morgan = require("morgan")
const app = express()
const mainRouter = require("./src/routes/index");
const cors = require('cors');

const {default: mongoose} = require('mongoose');
mongoose.connect(`${process.env.DB_URL}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
})
    .then(() => {
        console.log('Connected to MongoDB successfully!');
    })
    .catch((error) => {
        console.error('Error connecting to MongoDB:', error);
    });

app.use(morgan("dev"))

// const corsOptions = {
//   origin: 'http://localhost', // ganti dengan URL frontend Anda
//   optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
// }

app.use(cors())

app.use(bodyParser.json())

app.use("/", mainRouter)
app.all("*", (req, res, next) => {
    response(res, 404, false, null, "404!")
})
app.get("/", (req, res, next) => {
    res.status(200).json({status: "success", statusCode: 200});
});

const port = process.env.PORT

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})