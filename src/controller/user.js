const {response} = require("../middleware/common");
const bcrypt = require('bcryptjs');
const userMongo = require("../model/mongodb/User")
const cleanerDetailMongo = require("../model/mongodb/CleanerDetail")
const bson = require('bson');

const UsersController = {
    register: async (req, res, next) => {
        let nik = req.body.nik
        let user = await cleanerDetailMongo.findOne({nik})
        if (user) {
            return response(res, 404, false, null, "cleaner account actived")
        }
        if (req.body.password !== req.body.password_confirm) {
            return response(res, 404, false, null, "password not same")
        }
        const hashedPassword = bcrypt.hashSync(req.body.password);
        let data = {
            role: "Kilapin Cleaner",
            name: req.body.name,
            password: hashedPassword,
            phone: req.body.phone,
            email: req.body.email,
            photo_user: req.body.photo_user,
            date_birth: req.body.date_birth,
            address: req.body.address,
            postal_code: req.body.postal_code,
            cleaner_detail: new bson.ObjectId(),
        }
        let createUser = new userMongo(data);
        let newUser = await createUser.save();
        try {
            let cleanerDetail = {
                nik: req.body.nik,
                domisili: req.body.domisili,
                education: req.body.education,
                photo_ktp: req.body.photo_ktp,
                no_npwp: req.body.no_npwp,
                foto_npwp: req.body.foto_npwp,
                foto_ijazah: req.body.foto_ijazah,
                bank: req.body.bank,
                no_bank: req.body.no_bank,
                no_sertif_bnsp: req.body.no_sertif_bnsp,
                sertif_bnsp: req.body.sertif_bnsp,
                no_sim_c: req.body.no_sim_c,
                foto_sim_c: req.body.foto_sim_c,
                status: "Ready",
                _id: newUser.cleaner_detail
            }
            let createDetail = new cleanerDetailMongo(cleanerDetail);
            let newDetail = await createDetail.save();
            let result = await userMongo.findOne({_id: newUser.id})
                .populate("cleaner_detail")
            if (result) {
                console.log(result)
                response(res, 200, true, result, "signup cleaner success")
            }
        } catch (err) {
            console.log(err)
            response(res, 404, false, err, "signup cleaner fail")
        }
    }, login: async (req, res, next) => {
        try {
            const user = await userMongo.findOne({phone: req.body.phone})
            if (!user) {
                return response(res, 404, false, null, "phone number not found")
            }
            let password = req.body.password

            let validation = bcrypt.compareSync(password, user.password)
            if (!validation) {
                return response(res, 404, false, null, "wrong password")
            }
            response(res, 200, true, user, "login successfully")
        } catch (error) {
            response(res, 404, false, error, "login failed")
        }
    }, getAll: async (req, res, next) => {
        let result = await userMongo.find({role: "Kilapin Cleaner"}).populate("cleaner_detail");
        try {
            response(res, 200, true, result, "get all cleaner success")
        } catch (error) {
            response(res, 404, false, error, "get all cleaner fail")
        }
    }, getId: async (req, res, next) => {
        let paramsId = req.params.id
        let result = await userMongo.findOne({_id: paramsId, role: "Kilapin Cleaner"})
            .populate("cleaner_detail")
        try {
            response(res, 200, true, result, "get cleaner detail success")
        } catch (error) {
            response(res, 404, false, error, "get cleaner detail fail")
        }
    },
    passwordUpdate: async (req, res, next) => {
        let phone = req.body.phone
        const hashedPassword = bcrypt.hashSync(req.body.password);
        let result = await userMongo.updateOne({phone: phone, role: "Kilapin Cleaner"}, {$set: {password: hashedPassword}});
        try {
            response(res, 200, true, result, "Password updated")
        } catch (error) {
            response(res, 404, false, error, "Password update fail")
        }
    },
}

exports.UsersController = UsersController;