const {response} = require("../middleware/common");
// const ModelOrder = require("../model/order")
const orderMongo = require("../model/mongodb/Order")
const photoOrderMongo = require("../model/mongodb/PhotoOrder")
const serviceMongo = require("../model/mongodb/Service")
const categoryMongo = require("../model/mongodb/Category")
const cleanerDetailMongo = require("../model/mongodb/CleanerDetail");
const trackingCleaner = require("../model/mongodb/TrackingCleaner");
const userMongo = require("../model/mongodb/User");
const pusher = require("../config/pusher")

const OrderController = {
    getOrder: async (req, res, next) => {
        try {
            let filter = {
                cleaner_id: req.params.cleaner_id,
                status_payment: "Paid",
                status: { $ne: 'Done' }
            }
            let result = await orderMongo.findOne(filter)
                .populate("user_id")
                .populate("service_id")
            if (result) {
                response(res, 200, true, result, "Get order success")
            } else {
                response(res, 200, true, null, "No order found");
            }
        } catch (error) {
            response(res, 404, false, error, "Get order fail")
        }
    }, getOrderPaid: async (req, res, next) => {
        try {
            let filter = {
                status_payment: "Paid", postal_code: req.params.postal_code
            }
            let result = await orderMongo.find(filter)
                .populate("user_id")
                .populate("service_id")
                .sort({createdAt: -1})
            if (result && result.length > 0) {
                response(res, 200, true, result, "Get order success")
            } else {
                response(res, 200, true, null, "No order found");
            }
        } catch (error) {
            response(res, 404, false, error, "Get order fail")
        }
    }, postPhotoOrder: async (req, res, next) => {
        try {
            let data = {
                order_id: req.body.order_id, name: req.body.name, url: req.body.url
            }
            let createPhotoOrder = new photoOrderMongo(data);
            let result = await createPhotoOrder;
            result.save()
            response(res, 200, true, result, "Photo Order Uploaded")
        } catch (error) {
            response(res, 404, false, error, "Orders is done")
        }
    }, getOrderHistory: async (req, res, next) => {
        try {
            let filter = {
                cleaner_id: req.params.cleaner_id, status: "Done"
            }
            let result = await orderMongo.find(filter)
                .populate("user_id")
                .populate("service_id")
                .sort({createdAt: 1})
            if (result && result.length > 0) {
                response(res, 200, true, result, "Get order history success")
            } else {
                response(res, 200, true, null, "No order found");
            }
        } catch (error) {
            response(res, 404, false, error, "Get order fail")
        }
    }, postCleaner: async (req, res, next) => {
        const model = orderMongo
        const data = {
            id: req.params.id, order_id: req.body.order_id
        }
        try {
            await model.updateOne({order_id: data.order_id}, {$set: {status: 'Get-Cleaner', cleaner_id: data.id}})
            const updatedOrder = await model.findOne({order_id: data.order_id});
            if (updatedOrder) {
                response(res, 200, true, updatedOrder, "get cleaner success")
            } else {
                response(res, 404, false, null, "No order updated")
            }
        } catch (error) {
            response(res, 404, false, error, "get cleaner fail")
        }
    }, claimOrder: async (req, res, next) => {
        try {
            const order = await orderMongo.findOne({order_id: req.params.orderId});
            order.cleaner_status = 'Accepted';
            order.status = "Placement";
            order.cleaner_id = req.params.userId
            order.save();
            if(order.booking_type === "Urgent"){
                const cleaner = await userMongo.findOne({_id : order.cleaner_id}).populate("cleaner_detail");
                cleaner.cleaner_detail.status = "Ready Urgent";
                cleaner.cleaner_detail.save();
            }
            response(res, 200, true, order, "Claim Order Success")
        } catch (error) {
            console.log(error)
            response(res, 404, false, error, "Claim Order Fail")
        }
    }, detailOrder: async (req, res, next) => {
        try {
            let filter = {
                order_id: req.params.order_id,
            }
            let result = await orderMongo.findOne(filter)
                .populate("user_id")
                .populate("service_id")
            response(res, 200, true, result, "Get Detail Order Success")
        } catch (error) {
            console.log(error)
            response(res, 404, false, error, "Get Detail Order Fail")
        }
    },updateStatusUrgentOrder: async (req, res, next) => {
        try {
            const currentDate = new Date();

            const formattedDateNow = `${currentDate.getFullYear()}-${String(currentDate.getMonth() + 1).padStart(2, '0')}-${String(currentDate.getDate()).padStart(2, '0')} ${String(currentDate.getHours()).padStart(2, '0')}:${String(currentDate.getMinutes()).padStart(2, '0')}`;

            const data = {
                name: req.body.name,
                cleaner_id: req.body.cleaner_id,
                status: req.body.status,
                lat_long: req.body.lat_long,
                address: req.body.address,
                datetime: formattedDateNow,
            }
            const createStatus = new trackingCleaner(data);
            await createStatus.save()
            if (createStatus) {
                console.log(data)
                response(res, 200, true, data, "Status update success")
            }
        } catch (err) {
            console.log(err)
            response(res, 404, false, err, "Status update success")
        }
    },
    getTrackingAll : async (req, res, next) => {
        try {
            // Parse the start and end date from the request query or use default values
            const startDate = new Date(req.query.startDate)
            const endDate = new Date(req.query.endDate)

            // Construct the filter object using $gte and $lte operators
            let filter = {
                createdAt: {
                    $gte: startDate,
                    $lte: endDate,
                }
            };

            let result = await trackingCleaner.find(filter).sort({ createdAt: -1 });

            if (result.length > 0) {
                response(res, 200, true, result, "Get order success");
            } else {
                response(res, 200, true, null, "No order found");
            }
        } catch (error) {
            response(res, 404, false, error, "Get order fail")
        }
    },
    findingCleaner : async (req, res, next) => {
        let result;
        let order;
        console.log("Order Id : ", req.body.order_id);
        const orderId = req.body.order_id;
        console.log("Order ID: ", orderId);

        const list_cleaner = await userMongo.find({
            role: "Kilapin Cleaner"
        }).populate('cleaner_detail');
        console.log("List of Cleaners: ", list_cleaner);

        const filteredCleaners = list_cleaner.filter((cleaner) => cleaner.cleaner_detail && cleaner.cleaner_detail.status === "Ready");
        console.log("Filtered Cleaners: ", filteredCleaners);

        let isAccepted = false;
        const threeMinutes = 3 * 60 * 1000; // 3 minutes in milliseconds
        const endTime = Date.now() + threeMinutes; // Calculate the end time

        while (Date.now() < endTime) {
            // while (true) {
            let isCleanerAccepted = false;

            for (let i = 0; i < filteredCleaners.length; i++) {
                const cleaner = filteredCleaners[i];
                const cleanerId = cleaner._id;
                console.log(cleanerId)
                console.log("Searching Cleaner .........");

                // Update cleaner_id only if the cleaner has not been accepted
                if (!isCleanerAccepted) {
                    await orderMongo.updateOne({order_id: orderId}, {$set: {cleaner_id: cleanerId}});
                    console.log("Cleaner_id updated successfully");
                }
                // Wait for 20 seconds
                await new Promise(resolve => setTimeout(resolve, 20000));

                order = await orderMongo.findOne({order_id: orderId}).populate("user_id").populate("service_id");

                await pusher.trigger('order-on-going', `${cleanerId}`, order);

                if (order.cleaner_status === 'Finding') {
                    console.log("Checking cleaner_status after 20 seconds");
                    console.log("Current Cleaner ID: ", cleanerId);
                    const nextIndex = i + 1;
                    const nextCleaner = filteredCleaners[nextIndex];
                    const nextCleanerId = nextCleaner?._id;
                    console.log("Next Cleaner ID: ", nextCleanerId);

                    // If the next cleaner is not available, or cleaner status is rejected, update isCleanerAccepted flag
                    if (!nextCleanerId || order.cleaner_status === 'Rejected') {
                        isCleanerAccepted = false;
                        break; // Exit the loop
                    }
                } else if (order.cleaner_status === 'Rejected') {
                    console.log("Checking cleaner_status after 20 seconds");
                    console.log("Current Cleaner ID: ", cleanerId);
                    const nextIndex = i + 1;
                    const nextCleaner = filteredCleaners[nextIndex];
                    const nextCleanerId = nextCleaner?._id;
                    console.log("Next Cleaner ID: ", nextCleanerId);

                    // If the next cleaner is not available, update isCleanerAccepted flag
                    if (!nextCleanerId) {
                        isCleanerAccepted = false;
                        break; // Exit the loop
                    }
                } else if (order.cleaner_status === "Accepted") {
                    console.log("Order has been Accepted by: ", cleanerId);
                    isCleanerAccepted = true; // Set the flag to true once the cleaner is accepted
                    console.log("isCleanerAccepted: ", isCleanerAccepted); // Add this line
                    break; // Exit the loop
                }
            }

            console.log("isCleanerAccepted 2: ", isCleanerAccepted); // Add this line
            if (isCleanerAccepted) {
                console.log("Masuk sini !!!!!!");
                result = await orderMongo.findOne({order_id: orderId});
                isAccepted = true;
                break;
            }
            console.log("No cleaner accepted, repeating the loop or returning a response");
        }

        if (isAccepted) {
            console.log("Masuk if result : ", result);
            return response(res, 200, true, result, "Finding Cleaner success");
        } else {
            result = await orderMongo.updateOne({order_id: orderId}, {$set: {cleaner_status: "Cleaner not found"}});
            response(res, 203, true, result, "Cleaner not found");
        }
    },
    getOrderCleaner: async (req, res,next)=>{
        try {
            const selectField = ['order_id', 'cleaner_id', 'status', 'initial_price', 'cleaner_balance', 'status_payment', 'cleaner_status']
            const order = await orderMongo.find({
                status_payment:'Paid',
                status:'Done',
                cleaner_id:{ $ne: null}
            }).select(selectField);

            if (order) {
                response(res, 200, true, order, "Get All Order Done success")
            } else {
                response(res, 200, true, null, "Order not found");
            }
        } catch (error) {

            response(res, 404, false, error, "Get order fail")
        }
    } 
}

exports.OrderController = OrderController