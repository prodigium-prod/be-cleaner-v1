const express = require("express");
const router = express.Router();
const {OrderController} = require('../controller/order')

router.post("/writecleaner/:id",OrderController.postCleaner)
router.get("/get-order/:cleaner_id",OrderController.getOrder)
router.get("/get-order-paid/:postal_code",OrderController.getOrderPaid)
router.get("/history-order/:cleaner_id", OrderController.getOrderHistory)
router.post("/photo-order",OrderController.postPhotoOrder)
router.get("/claim-order/:orderId/:userId",OrderController.claimOrder)
router.get("/detail/:order_id",OrderController.detailOrder)
router.get("/tracking-cleaner",OrderController.getTrackingAll)
router.post("/update-status-urgent",OrderController.updateStatusUrgentOrder)
router.post("/finding-cleaner",OrderController.findingCleaner)
router.get("/api/get-order-done-cleaner",OrderController.getOrderCleaner);

module.exports = router;