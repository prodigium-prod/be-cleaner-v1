const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const serviceSchema = new mongoose.Schema({
    name: {type: String, required: true},
    description: {type: String},
    option: {type: String, required: true},
    price: {type: String, default: null}
}, {timestamps: true});

const Service = mongoose.model('Service', serviceSchema);

module.exports = Service;
