const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const photoOrderSchema = new mongoose.Schema({
    order_id: {type: String, default: null},
    name: {type: String, default: null},
    url: {type: String, default: null}
}, { timestamps: true });

const PhotoOrder = mongoose.model('PhotoOrder', photoOrderSchema);

module.exports = PhotoOrder;