const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const categorySchema = new mongoose.Schema({
    name: {type: String, required: true},
}, {timestamps: true});

const Category = mongoose.model('Category', categorySchema);
module.exports = Category;