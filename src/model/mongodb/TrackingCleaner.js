const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const trackingCleanerSchema = new mongoose.Schema({
    name: {type: String, required: true},
    cleaner_id: {type: String, required: true},
    status: {type: String, required: true},
    lat_long: {type: String, default: null},
    address: {type: String, default: null},
    datetime: {type: String, required: true},
}, {timestamps: true});

const TrackingCleaner = mongoose.model('TrackingCleaner', trackingCleanerSchema);
module.exports = TrackingCleaner;