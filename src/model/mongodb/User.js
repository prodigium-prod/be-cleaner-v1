const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const userSchema = new mongoose.Schema({
    name: {type: String, default: null},
    password: {type: String, default: null},
    phone: {type: String, default: null},
    role: {type: String, default: null},
    email: {type: String, default: null},
    date_birth: {type: String, default: null},
    photo_user: {type: String, default: null},
    address: {type: String, default: null},
    member: {type: String, default: null},
    referral_code: {type: String, default: null},
    point: {type: Number, default: 0},
    auth: {type: Number, default: 0},
    voucher: {type: String, default: null},
    postal_code: {type: Number, default: null},
    cleaner_detail: {type: mongoose.Schema.Types.ObjectId, ref: 'CleanerDetail'},
}, {timestamps: true});

const User = mongoose.model('User', userSchema);

module.exports = User;
