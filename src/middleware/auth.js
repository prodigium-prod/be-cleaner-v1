const jwt = require('jsonwebtoken')

let key = process.env.JWT_KEY

const generateToken = (payload) =>{
    const verifyOpts = {
        expiresIn : '7d'
    }
    return jwt.sign(payload, key, verifyOpts)
}

module.exports = {generateToken}